module InCoin::in_coin {
    use std::string::utf8;
    use std::signer;
    use std::error;
    use std::option::{Self};

    use aptos_framework::coin::{Self, MintCapability, BurnCapability};

    const ENO_CAPABILITIES: u64 = 101;
    const ESUPPLY_LIMIT_EXCEEDED: u64 = 102;

    const SUPPLY_LIMIT: u128 = 100000000 * 100000000;
    
    struct InCoin has key {}

    struct Capabilities<phantom CoinType> has key {
        mint_cap: MintCapability<CoinType>,
        burn_cap: BurnCapability<CoinType>,
    }
    
    fun init_module(sender: &signer) {
        let (
            _burn_cap,
            _freeze_cap,
            _mint_cap
        ) = coin::initialize<InCoin>(
            sender,
            utf8(b"IN"),
            utf8(b"Tin"),
            8,
            true,
        );
        move_to(sender, Capabilities<InCoin> {
            mint_cap: _mint_cap,
            burn_cap: _burn_cap,
        });
        coin::destroy_freeze_cap(_freeze_cap);
    }

    public entry fun mint(
        account: &signer,
        dst_addr: address,
        amount: u64,
    ) acquires Capabilities {
        let account_addr = signer::address_of(account);
        assert!(
            exists<Capabilities<InCoin>>(account_addr),
            error::not_found(ENO_CAPABILITIES),
        );
        let capabilities = borrow_global<Capabilities<InCoin>>(account_addr);
        let coins_minted = coin::mint(amount, &capabilities.mint_cap);
        coin::deposit(dst_addr, coins_minted);

        let supply_after = coin::supply<InCoin>();

        if (option::is_some(&supply_after)) {
            let supply_value = option::extract(&mut supply_after);
            assert!(
                SUPPLY_LIMIT >= supply_value,
                error::out_of_range(ESUPPLY_LIMIT_EXCEEDED),
            );
        }
    }

    public entry fun burn(
        account: &signer,
        amount: u64,
    ) acquires Capabilities {
        let account_addr = signer::address_of(account);
        assert!(
            exists<Capabilities<InCoin>>(account_addr),
            error::not_found(ENO_CAPABILITIES),
        );
        let capabilities = borrow_global<Capabilities<InCoin>>(account_addr);
        let to_burn = coin::withdraw<InCoin>(account, amount);
        coin::burn(to_burn, &capabilities.burn_cap);
    }

    public entry fun register(account: &signer) {
        coin::register<InCoin>(account);
    }
}
