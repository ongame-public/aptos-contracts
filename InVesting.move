module InVesting::in_vesting {
    use std::signer;    
    use std::error; 

    use aptos_framework::resource_account;
    use aptos_framework::account;
    use aptos_framework::coin;
    use aptos_framework::timestamp;

    use aptos_std::type_info;
    use aptos_std::table_with_length::{ Self as Table, TableWithLength };

    use InCoin::in_coin::{ InCoin as VestingCoin };

    //
    // Metadata
    //

    struct VestingStorage has key {
        vesting_list: TableWithLength<address, VestingStruct>,
        signer_cap: account::SignerCapability,
    }

    struct VestingStruct has store {
        total_amount: u64,
        last_claimed: u64,
        residue: u64,
    }

    //
    // Errors
    //

    const EINCORRECT_AMOUNT: u64 = 201;
    const EALREADY_VESTED: u64 = 202;
    const ENOT_VESTED: u64 = 203;
    const ENOTHING_TO_RELEASE: u64 = 204;
    const ENO_TIME_HAS_PASSED: u64 = 205;

    //
    // Constants
    //

    const ADDRESS_THIS: address = @InVesting;
    const SOURCE_ADDRESS: address = @source_addr;
    const VESTING_PERCENT: u64 = 25;  // 2.5% every month
    const DENOMINATOR: u64 = 1000;
    const MONTH_IN_SECONDS: u64 = 2592000;

    fun init_module(resource_signer: &signer) {

        let vesting_signer_cap = resource_account::retrieve_resource_account_cap(resource_signer, SOURCE_ADDRESS);
        coin::register<VestingCoin>(resource_signer);

        let resource_signer_with_cap = account::create_signer_with_capability(&vesting_signer_cap);
        coin::register<VestingCoin>(&resource_signer_with_cap);

        move_to(resource_signer, VestingStorage {
            vesting_list: Table::new(),
            signer_cap: vesting_signer_cap
        });
    }

    public entry fun vesting_funds(
        account: &signer,
        receiver: address,
        total_amount: u64
    ) acquires VestingStorage {

        assert!(
            total_amount > 0,
            error::invalid_argument(EINCORRECT_AMOUNT)
        );

        let vesting_pool = borrow_global_mut<VestingStorage>(ADDRESS_THIS);        
        assert!(
            !Table::contains(&vesting_pool.vesting_list, receiver),
            error::already_exists(EALREADY_VESTED)
        );

        coin::transfer<VestingCoin>(account, ADDRESS_THIS, total_amount);
        
        Table::add(&mut vesting_pool.vesting_list, receiver, VestingStruct {
            total_amount: total_amount,
            last_claimed: timestamp::now_seconds() + MONTH_IN_SECONDS,
            residue: total_amount,
        });
    }

    public entry fun release_funds(
        receiver: &signer,
    ) acquires VestingStorage {

        let receiver_address = signer::address_of(receiver);
        let vesting_pool = borrow_global_mut<VestingStorage>(ADDRESS_THIS); 

        assert!(
            Table::contains(&vesting_pool.vesting_list, receiver_address),
            error::not_found(ENOT_VESTED)
        );

        let user_info = Table::borrow_mut<address, VestingStruct>(&mut vesting_pool.vesting_list, receiver_address);
        let now = timestamp::now_seconds();

        assert!(
            user_info.residue > 0,
            error::resource_exhausted(ENOTHING_TO_RELEASE)
        );

        assert!(
            now > user_info.last_claimed,
            error::permission_denied(ENO_TIME_HAS_PASSED)
        );

        let n = ((now - user_info.last_claimed) / MONTH_IN_SECONDS) + 1;
        let to_claim = (user_info.total_amount * VESTING_PERCENT * n) / DENOMINATOR;

        if (to_claim > user_info.residue) {
            to_claim = user_info.residue;
        };

        user_info.residue = user_info.residue - to_claim;
        user_info.last_claimed = user_info.last_claimed + (MONTH_IN_SECONDS * n);

        if (to_claim > 0) {

            if (!coin::is_account_registered<VestingCoin>(receiver_address)) {
                coin::register<VestingCoin>(receiver);
            };

            let resource_signer = account::create_signer_with_capability(&vesting_pool.signer_cap);
            coin::transfer<VestingCoin>(&resource_signer, receiver_address, to_claim);
        };
    }

    fun coin_address(): address {
        let type_info = type_info::type_of<VestingCoin>();
        type_info::account_address(&type_info)
    }

    #[view]
    public fun residue(account: address): u64 acquires VestingStorage {
        let vesting_pool = borrow_global<VestingStorage>(ADDRESS_THIS); 

        assert!(
            Table::contains(&vesting_pool.vesting_list, account),
            error::not_found(ENOT_VESTED)
        );
        let user_info = Table::borrow<address, VestingStruct>(&vesting_pool.vesting_list, account);
        user_info.residue
    }

    #[view]
    public fun next_release(account: address): u64 acquires VestingStorage {
        let vesting_pool = borrow_global<VestingStorage>(ADDRESS_THIS); 

        assert!(
            Table::contains(&vesting_pool.vesting_list, account),
            error::not_found(ENOT_VESTED)
        );
        let user_info = Table::borrow<address, VestingStruct>(&vesting_pool.vesting_list, account);
        user_info.last_claimed
    }

    #[view]
    public fun total_vested(account: address): u64 acquires VestingStorage {
        let vesting_pool = borrow_global<VestingStorage>(ADDRESS_THIS); 

        assert!(
            Table::contains(&vesting_pool.vesting_list, account),
            error::not_found(ENOT_VESTED)
        );
        let user_info = Table::borrow<address, VestingStruct>(&vesting_pool.vesting_list, account);
        user_info.total_amount
    }
}
