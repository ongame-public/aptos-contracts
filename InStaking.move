module InStaking::in_staking {
    use std::signer;    
    use std::error; 
    use std::vector;

    use aptos_framework::resource_account;
    use aptos_framework::account;
    use aptos_framework::coin;
    use aptos_framework::timestamp;

    use aptos_std::type_info;
    use aptos_std::table_with_length::{ Self as Table, TableWithLength };

    use InCoin::in_coin::{ InCoin as StakingCoin };

    //
    // Metadata
    //

    struct StakingStorage has key {
        signer_cap: account::SignerCapability,
        treasury_balance: u64,
        reserved_balance: u64,
        lock_periods_to_percent: TableWithLength<u64, u64>,
    }

    struct UserStakes has key {
        stakes_list: TableWithLength<u64, Stake>,
        stakes_indexes: vector<u64>,
        next_index: u64,
    }

    struct Stake has store, drop {
        lock_period: u64,
        start_timestamp: u64,
        last_claimed_timestamp: u64,
        rewards_percent: u64,
        staked_amount: u64,
        total_claimed: u64,
    }

    //
    // Errors
    //

    const EINCORRECT_STAKE_AMOUNT: u64 = 301;
    const EINVALID_LOCK_PERIOD: u64 = 302;
    const ENOT_ENOUGH_REWARDS_IN_TREASURY: u64 = 303;
    const EINCORRECT_STAKE_INDEX: u64 = 304;
    const ENO_TIME_HAS_PASSED: u64 = 305;

    //
    // Constants
    //

    const ADDRESS_THIS: address = @InStaking;
    const SOURCE_ADDRESS: address = @source_addr;
    const MIN_STAKE_AMOUNT: u64 = 100;
    const DENOMINATOR: u64 = 100;
    
    const MONTH_IN_SECONDS: u64 = 2592000;

    fun init_module(resource_signer: &signer) acquires StakingStorage {

        let staking_signer_cap = resource_account::retrieve_resource_account_cap(resource_signer, SOURCE_ADDRESS);
        coin::register<StakingCoin>(resource_signer);

        let resource_signer_with_cap = account::create_signer_with_capability(&staking_signer_cap);
        coin::register<StakingCoin>(&resource_signer_with_cap);

        move_to(resource_signer, StakingStorage {
            signer_cap: staking_signer_cap,
            treasury_balance: 0,
            reserved_balance: 0,
            lock_periods_to_percent: Table::new()
        });

        let staking = borrow_global_mut<StakingStorage>(ADDRESS_THIS);
        Table::add(
            &mut staking.lock_periods_to_percent, 
            MONTH_IN_SECONDS,
            1 
        );
        Table::add(
            &mut staking.lock_periods_to_percent, 
            6 * MONTH_IN_SECONDS,
            2 
        );
        Table::add(
            &mut staking.lock_periods_to_percent, 
            12 * MONTH_IN_SECONDS,
            4 
        );
    }

    public entry fun replenish_the_treasury(
        sender: &signer,
        amount: u64
    ) acquires StakingStorage {
        let staking = borrow_global_mut<StakingStorage>(ADDRESS_THIS);
        coin::transfer<StakingCoin>(sender, ADDRESS_THIS, amount);
        staking.treasury_balance = staking.treasury_balance + amount;
    }

    public entry fun stake(
        sender: &signer,
        period: u64,
        amount: u64
    ) acquires StakingStorage, UserStakes {

        assert!(
            amount >= MIN_STAKE_AMOUNT,
            error::invalid_argument(EINCORRECT_STAKE_AMOUNT)
        );

        let staking = borrow_global_mut<StakingStorage>(ADDRESS_THIS);
        assert!(
            Table::contains(&staking.lock_periods_to_percent, period),
            error::invalid_argument(EINVALID_LOCK_PERIOD)
        );

        let percent = *Table::borrow<u64, u64>(&staking.lock_periods_to_percent, period);
        let total_rewards = (amount * period * percent) / (MONTH_IN_SECONDS * DENOMINATOR);

        assert!(
            staking.treasury_balance - staking.reserved_balance >= total_rewards,
            error::resource_exhausted(ENOT_ENOUGH_REWARDS_IN_TREASURY)
        );

        coin::transfer<StakingCoin>(sender, ADDRESS_THIS, amount);
        staking.reserved_balance = staking.reserved_balance + total_rewards;

        let sender_address = signer::address_of(sender);
        if (!exists<UserStakes>(sender_address)) {
            move_to(sender, UserStakes {
                stakes_list: Table::new(),
                stakes_indexes: vector::empty(),
                next_index: 0,
            });
        };

        let now = timestamp::now_seconds();
        let user_stakes_info = borrow_global_mut<UserStakes>(sender_address);
        Table::add(&mut user_stakes_info.stakes_list, user_stakes_info.next_index, Stake {
            lock_period: period,
            start_timestamp: now,
            last_claimed_timestamp: now + MONTH_IN_SECONDS,
            rewards_percent: percent,
            staked_amount: amount,
            total_claimed: 0,
        });
        vector::push_back(&mut user_stakes_info.stakes_indexes, user_stakes_info.next_index);
        user_stakes_info.next_index = user_stakes_info.next_index + 1;
    }   

    public entry fun claim(
        sender: &signer,
        stake_index: u64
    ) acquires StakingStorage, UserStakes {

        let sender_address = signer::address_of(sender);
        let user_stakes_info = borrow_global_mut<UserStakes>(sender_address);

        let (includes, index) = vector::index_of(&user_stakes_info.stakes_indexes, &stake_index);
        assert!(
            includes,
            error::out_of_range(EINCORRECT_STAKE_INDEX)
        );  

        let stake_info = Table::borrow_mut<u64, Stake>(&mut user_stakes_info.stakes_list, stake_index);
        let now = timestamp::now_seconds();
        assert!(
            now > stake_info.last_claimed_timestamp,
            error::permission_denied(ENO_TIME_HAS_PASSED)
        );
        
        let n = ((now - stake_info.last_claimed_timestamp) / MONTH_IN_SECONDS) + 1;
        let rewards = (stake_info.staked_amount * stake_info.rewards_percent * n) / DENOMINATOR;
        let max_rewards = (stake_info.staked_amount * stake_info.rewards_percent * (stake_info.lock_period / MONTH_IN_SECONDS)) / DENOMINATOR;
        let residue = max_rewards - stake_info.total_claimed;

        if ((rewards >= residue) || (stake_info.lock_period + stake_info.start_timestamp >= now)) {
            rewards = residue + stake_info.staked_amount;

            Table::remove(&mut user_stakes_info.stakes_list, stake_index);
            vector::remove(&mut user_stakes_info.stakes_indexes, index);
        } else {
            stake_info.total_claimed = stake_info.total_claimed + rewards;
            stake_info.last_claimed_timestamp = stake_info.last_claimed_timestamp + (MONTH_IN_SECONDS * n);
        };

        if (rewards > 0) {
            if (!coin::is_account_registered<StakingCoin>(sender_address)) {
                coin::register<StakingCoin>(sender);
            };

            let staking = borrow_global_mut<StakingStorage>(ADDRESS_THIS); 
            let resource_signer = account::create_signer_with_capability(&staking.signer_cap);
            coin::transfer<StakingCoin>(&resource_signer, sender_address, rewards);
        };
    }   

    fun coin_address(): address {
        let type_info = type_info::type_of<StakingCoin>();
        type_info::account_address(&type_info)
    }

    #[view]
    public fun get_stakes_indexes(
        account: address
    ): vector<u64> acquires UserStakes {
        let user_stakes_info = borrow_global<UserStakes>(account);
        user_stakes_info.stakes_indexes
    }

    #[view]
    public fun next_claim(
        account: address, 
        stake_index: u64
    ): u64 acquires UserStakes  {
        let user_stakes_info = borrow_global<UserStakes>(account);

        let (includes, _) = vector::index_of(&user_stakes_info.stakes_indexes, &stake_index);
        assert!(
            includes,
            error::out_of_range(EINCORRECT_STAKE_INDEX)
        );

        let stake_info = Table::borrow<u64, Stake>(&user_stakes_info.stakes_list, stake_index);
        stake_info.last_claimed_timestamp
    }

    #[view]
    public fun until_unlock(
        account: address, 
        stake_index: u64
    ): u64 acquires UserStakes  {
        let user_stakes_info = borrow_global<UserStakes>(account);

        let (includes, _) = vector::index_of(&user_stakes_info.stakes_indexes, &stake_index);
        assert!(
            includes,
            error::out_of_range(EINCORRECT_STAKE_INDEX)
        );

        let stake_info = Table::borrow<u64, Stake>(&user_stakes_info.stakes_list, stake_index);
        let now = timestamp::now_seconds();
        let unlock_timestamp = stake_info.start_timestamp + stake_info.lock_period;
        let to_return = 0;
        if (unlock_timestamp > now) {
            to_return = unlock_timestamp - now;
        };
        to_return
    }
}
